package controller_tests;

import my.homework.Application;
import my.homework.offices.OfficesModel;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import test_helpers.Utils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class OfficesTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager em;

    private MockMvc mockMvc;
    private String success = "success";
    private String error = "error";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getOfficeByOrganizationId() throws Exception {
        int organizationId = 1;
        int officePhone = 9546;
        JSONObject body = new JSONObject();
        body.put("phone", officePhone);

        mockMvc
                .perform(post("/api/office/list/" + organizationId)
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data[0].organization.id", is(organizationId)))
                .andExpect(jsonPath("$.data[0].phone", is(officePhone)));
    }

    @Test
    public void getOfficeById() throws Exception {
        int officeId = 5;
        String officeName = "Second office";
        mockMvc
                .perform(get("/api/office/" + officeId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data.id", is(officeId)))
                .andExpect(jsonPath("$.data.name", is(officeName)));
    }

    @Test
    public void updateOffice() throws Exception {
        int officeId = 3;
        String newName = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("id", officeId);
        body.put("name", newName);
        mockMvc
                .perform(post("/api/office/update")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        OfficesModel office = (OfficesModel) em
                .createNativeQuery("select * from offices where id =" + officeId, OfficesModel.class).getSingleResult();
        Assert.assertEquals(newName, office.getName());
    }

    @Test
    public void deleteOffice() throws Exception {
        int officeId = 2;
        JSONObject body = new JSONObject();
        body.put("id", officeId);
        mockMvc
                .perform(post("/api/office/delete")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        try {
            OfficesModel office = (OfficesModel) em
                    .createNativeQuery("select * from offices where id =" + officeId, OfficesModel.class).getSingleResult();
            Assert.fail("Office not deleted");
        } catch (NoResultException ex) {
        }
    }

    @Test
    public void createNewOffice() throws Exception {
        int organizationId = 1;
        String newOfficeName = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("organizationId", organizationId);
        body.put("name", newOfficeName);

        mockMvc
                .perform(post("/api/office/save")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));

        OfficesModel newOffice = (OfficesModel) em.
                createNativeQuery("select * from offices order by id desc limit 1", OfficesModel.class).getSingleResult();
        Assert.assertEquals(newOfficeName, newOffice.getName());
    }
}
