package controller_tests;

import my.homework.Application;
import my.homework.employees.EmployeeModel;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import test_helpers.Utils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.http.MediaType.APPLICATION_JSON_UTF8;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class EmployeesTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager em;

    private MockMvc mockMvc;
    private String success = "success";
    private String error = "error";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void getEmployeesList() throws Exception {
        int office = 1;
        String position = "Programmer";
        JSONObject body = new JSONObject();
        body.put("position", position);
        body.put("office", office);

        mockMvc
                .perform(post("/api/employee/list/")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data[0].position", is(position)));
    }

    @Test
    public void getEmployeeById() throws Exception {
        int employeeId = 1;
        mockMvc
                .perform(get("/api/employee/" + employeeId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data.id", is(employeeId)));
    }

    @Test
    public void updateEmployee() throws Exception {
        int employeeId = 2;
        String firstName = Utils.getUniqueNameForTest();
        String lastName = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("firstName", firstName);
        body.put("lastName", lastName);
        body.put("id", employeeId);

        mockMvc
                .perform(post("/api/employee/update/")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        EmployeeModel employee = (EmployeeModel) em.
                createNativeQuery("select * from employees where id=" + employeeId, EmployeeModel.class).getSingleResult();
        Assert.assertEquals(firstName, employee.getFirstName());
        Assert.assertEquals(lastName, employee.getLastName());
    }

    @Test
    public void deleteEmployee() throws Exception {
        int employeeId = 5;
        JSONObject body = new JSONObject();
        body.put("id", employeeId);

        mockMvc
                .perform(post("/api/employee/delete/")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        try {
            EmployeeModel deletedEmployee = (EmployeeModel) em.
                    createNativeQuery("select * from employees where id=" + employeeId, EmployeeModel.class).getSingleResult();
            Assert.fail("Employee not deleted");
        } catch (NoResultException ex) {
        }
    }

    @Test
    public void createNewEmployee() throws Exception {
        String firstName = Utils.getUniqueNameForTest();
        String position = "Kek";
        JSONObject body = new JSONObject();
        body.put("firstName", firstName);
        body.put("position", position);

        mockMvc
                .perform(post("/api/employee/save/")
                        .contentType(APPLICATION_JSON_UTF8)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        EmployeeModel newEmployee = (EmployeeModel) em.
                createNativeQuery("select * from employees order by 1 desc limit 1", EmployeeModel.class).getSingleResult();
        Assert.assertEquals(firstName, newEmployee.getFirstName());
        Assert.assertEquals(position, newEmployee.getPosition());
    }
}
