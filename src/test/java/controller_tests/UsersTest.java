package controller_tests;

import my.homework.Application;
import my.homework.users.UsersModel;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import test_helpers.Utils;

import javax.persistence.EntityManager;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class UsersTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager em;

    private MockMvc mockMvc;
    private String success = "success";
    private String error = "error";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void registerNewUser() throws Exception {
        String newLogin = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("login", newLogin);
        body.put("password", "test");
        body.put("name", "Test name");

        mockMvc
                .perform(post("/api/register")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));

        String query = "select * from users order by id desc limit 1";
        UsersModel newUser = (UsersModel) em.createNativeQuery(query, UsersModel.class).getResultList().get(0);
        Assert.assertEquals(newLogin, newUser.getLogin());
    }

    @Test
    public void activateUser() throws Exception {
        String activationCodeFromDB = "12345678";

        mockMvc
                .perform(get("/api/activation?code=" + activationCodeFromDB))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
    }

    @Test
    public void activateUserNegative() throws Exception {
        String activationCodeFromDB = "lol";

        mockMvc
                .perform(get("/api/activation?code=" + activationCodeFromDB))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(error)));
    }

    @Test
    public void loginByUser() throws Exception {
        JSONObject body = new JSONObject();
        body.put("login", "test");
        body.put("password", "test");

        mockMvc
                .perform(post("/api/login")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
    }

    @Test
    public void loginByUserNegative() throws Exception {
        JSONObject body = new JSONObject();
        body.put("login", "lol");
        body.put("password", "kek");

        mockMvc
                .perform(post("/api/login")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(error)));
    }
}
