package controller_tests;

import my.homework.Application;
import my.homework.organizations.OrganizationsModel;
import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import test_helpers.Utils;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;

import static org.hamcrest.Matchers.is;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class OrganizationsTest {

    @Autowired
    private WebApplicationContext webApplicationContext;

    @Autowired
    private EntityManager em;

    private MockMvc mockMvc;
    private String success = "success";
    private String error = "error";

    @Before
    public void setup() {
        this.mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    }

    @Test
    public void createNewOrganization() throws Exception {
        String newNameOrganization = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("name", newNameOrganization);

        mockMvc
                .perform(post("/api/organization/save")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));
        OrganizationsModel newOrganization = (OrganizationsModel) em.
                createNativeQuery("select * from organizations order by id desc limit 1", OrganizationsModel.class).getResultList().get(0);
        Assert.assertEquals(newNameOrganization, newOrganization.getName());
    }

    @Test
    public void getOrganizationInfo() throws Exception {
        long organizationId = 3;
        mockMvc
                .perform(get("/api/organization/" + organizationId))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data.name", is("Third organization")))
                .andExpect(jsonPath("$.data.fullName", is("OOO Third")))
                .andExpect(jsonPath("$.data.inn", is(3333)))
                .andExpect(jsonPath("$.data.kpp", is(3333)))
                .andExpect(jsonPath("$.data.address", is("Penza 3")));
    }

    @Test
    public void getListOfOrganization() throws Exception {
        String organizationName = "Third organization";
        int organizationId = 3;
        JSONObject body = new JSONObject();
        body.put("name", organizationName);
        mockMvc
                .perform(post("/api/organization/list")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)))
                .andExpect(jsonPath("$.data[0].id", is(organizationId)))
                .andExpect(jsonPath("$.data[0].name", is(organizationName)));
    }

    @Test
    public void updateOrganization() throws Exception {
        long organizationId = 1;
        String newOrganizationName = Utils.getUniqueNameForTest();
        JSONObject body = new JSONObject();
        body.put("id", organizationId);
        body.put("name", newOrganizationName);

        mockMvc
                .perform(post("/api/organization/update")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));

        OrganizationsModel organization = (OrganizationsModel) em
                .createNativeQuery("select * from organizations where id=1", OrganizationsModel.class).getResultList().get(0);
        Assert.assertEquals(newOrganizationName, organization.getName());
    }

    @Test
    public void deleteOrganization() throws Exception {
        long organizationId = 4;
        JSONObject body = new JSONObject();
        body.put("id", organizationId);

        mockMvc
                .perform(post("/api/organization/delete")
                        .contentType(APPLICATION_JSON)
                        .content(body.toString()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.result", is(success)));

        try {
            OrganizationsModel afterDeleting = (OrganizationsModel) em
                    .createNativeQuery("select * from organizations where id=" + organizationId, OrganizationsModel.class).getSingleResult();
            Assert.fail("Organization not deleted");
        } catch (NoResultException ex) {
        }
    }
}
