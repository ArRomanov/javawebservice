package service_tests;

import my.homework.Application;
import my.homework.dictionaries.CountriesModel;
import my.homework.dictionaries.DictService;
import my.homework.dictionaries.DictionaryDao;
import my.homework.dictionaries.DocumentsModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class DictionariesTest {

    @Autowired
    private DictionaryDao dao;

    @Autowired
    private EntityManager em;

    DictService dictService;
    String success;
    String error;

    @Before
    public void initService() {
        dictService = new DictService(dao);
        success = "success";
        error = "error";
    }

    @Test
    public void getAllDocsTypes() {
        List listOfDocs = (List) dictService.getAllTypesOfDocs().data;
        List listOfDocsFromDb = em
                .createNativeQuery("select * from types_of_documents", DocumentsModel.class)
                .getResultList();
        Assert.assertNotEquals(0, listOfDocs.size());
        Assert.assertEquals(listOfDocsFromDb.size(), listOfDocs.size());
    }

    @Test
    public void getAllCodesOfCountries() {
        List listOfCountries = (List) dictService.getAllCodesOfCountries().data;
        List listOfCountriesFromDb = em
                .createNativeQuery("select * from countries_list", CountriesModel.class)
                .getResultList();
        Assert.assertNotEquals(0, listOfCountries.size());
        Assert.assertEquals(listOfCountriesFromDb.size(), listOfCountries.size());
    }
}
