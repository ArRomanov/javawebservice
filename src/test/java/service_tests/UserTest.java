package service_tests;

import my.homework.Application;
import my.homework.users.UsersDao;
import my.homework.users.UsersService;
import my.homework.users.UsersView;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class UserTest {

    @Autowired
    private UsersDao dao;

    UsersService userService;
    String success;
    String error;

    @Before
    public void initService() {
        userService = new UsersService(dao);
        success = "success";
        error = "error";
    }

    @Test
    @Transactional
    public void registerNewUser() {
        UsersView view = new UsersView();
        view.login = "NewTestUser";
        view.name = "newName";
        view.password = "newPassword";
        String result = userService.register(view).result;
        Assert.assertEquals(success, result);
    }

    @Test
    @Transactional
    public void activationOfUser() {
        String activationCodeFromDB = "12345678";
        String result = userService.activateUser(activationCodeFromDB).result;
        Assert.assertEquals(success, result);
    }

    @Test
    @Transactional
    public void activationOfUserNegative() {
        String wrongActivationCode = "1234567lol";
        String result = userService.activateUser(wrongActivationCode).result;
        Assert.assertEquals(error, result);
    }

    @Test
    public void checkLogIn() {
        UsersView view = new UsersView();
        view.login = "test";
        view.password = "test";
        String result = userService.logIn(view).result;
        Assert.assertEquals(success, result);
    }

    @Test
    public void checkLogInNegative() {
        UsersView view = new UsersView();
        view.login = "lol";
        view.password = "kek";
        String result = userService.logIn(view).result;
        Assert.assertEquals(error, result);
    }

}
