package service_tests;

import my.homework.Application;
import my.homework.employees.EmployeeModel;
import my.homework.employees.EmployeeService;
import my.homework.employees.EmployeeView;
import my.homework.employees.EmployeesDao;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class EmployeeTest {

    @Autowired
    private EmployeesDao dao;

    @Autowired
    private EntityManager em;

    EmployeeService employeeService;

    @Before
    public void initService() {
        employeeService = new EmployeeService(dao);
    }

    @Test
    public void getEmployee() {
        long employeeId = 1;
        EmployeeView employee = (EmployeeView) employeeService.getEmployeeById(employeeId).data;
        Assert.assertEquals(employee.id, employeeId);
    }

    @Test
    public void getListOfEmployees() {
        long employeeId = 1;
        String position = "Programmer";

        EmployeeView view = new EmployeeView();
        view.position = position;
        view.office = 1;
        List<EmployeeModel> listOfEmployee = (List<EmployeeModel>) employeeService.getListOfEmployees(view).data;
        EmployeeModel firstEmployee = listOfEmployee.get(0);
        Assert.assertEquals(employeeId, firstEmployee.getId());
        Assert.assertEquals(position, firstEmployee.getPosition());
    }

    @Test
    @Transactional
    public void deleteEmployee() {
        long employeeId = 3;
        EmployeeView employee = (EmployeeView) employeeService.getEmployeeById(employeeId).data;
        Assert.assertEquals(employeeId, employee.id);
        String result = employeeService.deleteOfEmployee(employeeId).result;
        Assert.assertEquals("success", result);
        List listOfEmployees = em.createNativeQuery("select * from employees where id = " + employeeId, EmployeeModel.class).getResultList();
        Assert.assertEquals(0, listOfEmployees.size());
    }

    @Test
    @Transactional
    public void updateEmployee() {
        long employeeId = 1;
        String newPositionForTest = "New position";
        EmployeeView employee = (EmployeeView) employeeService.getEmployeeById(employeeId).data;
        employee.position = newPositionForTest;
        employeeService.updateEmployee(employee);
        employee = (EmployeeView) employeeService.getEmployeeById(employeeId).data;
        Assert.assertEquals(newPositionForTest, employee.position);
    }

    @Test
    @Transactional
    public void createNewEmployee() {
        String nameOfNewEmployee = "New employee's name";

        EmployeeView view = new EmployeeView();
        view.firstName = nameOfNewEmployee;
        employeeService.addNewEmployee(view);
        List listOfEmployees = em.createNativeQuery("select * from employees order by id desc", EmployeeModel.class).getResultList();
        EmployeeModel employee = (EmployeeModel) listOfEmployees.get(0);
        Assert.assertEquals(nameOfNewEmployee, employee.getFirstName());
    }
}
