package service_tests;

import my.homework.Application;
import my.homework.offices.OfficeDao;
import my.homework.offices.OfficeService;
import my.homework.offices.OfficeView;
import my.homework.offices.OfficesModel;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class OfficeTest {

    @Autowired
    private OfficeDao dao;

    OfficeService officeService;
    String success;
    String error;

    @Before
    public void initService() {
        officeService = new OfficeService(dao);
        success = "success";
        error = "error";
    }

    @Test
    public void getOfficesList() {
        long organizationId = 1;
        int phone = 9546;
        OfficeView view = new OfficeView();
        view.phone = phone;
        List listOfOffices = (List) officeService.getListOfOffices(organizationId, view).data;
        Assert.assertEquals(1, listOfOffices.size());
        OfficesModel office = (OfficesModel) listOfOffices.get(0);
        Assert.assertEquals(phone, office.getPhone());
    }

    @Test
    public void getOfficeById() {
        long officeId = 1;
        OfficeView offices = (OfficeView) officeService.getOfficeById(1).data;
        Assert.assertEquals(officeId, offices.id);
    }

    @Test
    @Transactional
    public void updateOfficeInfo() {
        long officeId = 4;
        String newAddress = "Test address";
        OfficeView view = new OfficeView();
        view.id = officeId;
        view.address = newAddress;
        String responseResult = officeService.updateOfficeInfo(view).result;
        Assert.assertEquals(success, responseResult);
    }

    @Test
    @Transactional
    public void deleteOffice() {
        long officeId = 2;
        String responseResult = officeService.deleteOffice(officeId).result;
        Assert.assertEquals(success, responseResult);
    }

    @Test
    @Transactional
    public void createNewOffice() {
        long organizationId = 1;
        String officeName = "Test office";
        OfficeView view = new OfficeView();
        view.organizationId = organizationId;
        view.name = officeName;
        String responseResult = officeService.createNewOffice(view).result;
        Assert.assertEquals(success, responseResult);
    }
}
