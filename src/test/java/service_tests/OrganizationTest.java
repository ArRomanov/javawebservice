package service_tests;

import my.homework.Application;
import my.homework.organizations.OrganizationsDao;
import my.homework.organizations.OrganizationsModel;
import my.homework.organizations.OrganizationsService;
import my.homework.organizations.OrganizationsView;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.AutoConfigureTestEntityManager;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.transaction.annotation.Transactional;
import test_helpers.Utils;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {Application.class})
@WebAppConfiguration(value = "src/main/resources")
@AutoConfigureTestEntityManager
public class OrganizationTest {

    @Autowired
    private OrganizationsDao dao;

    OrganizationsService orgService;
    String success;
    String error;

    @Before
    public void initService() {
        orgService = new OrganizationsService(dao);
        success = "success";
        error = "error";
    }

    @Test
    @Transactional
    public void createNewOrganization() {
        String organizationName = Utils.getUniqueNameForTest();
        OrganizationsView view = new OrganizationsView();
        view.name = organizationName;
        String result = orgService.createNewOrganization(view).result;
        Assert.assertEquals(success, result);
    }

    @Test
    public void getOrganizationInfo() {
        long organizationId = 1;
        OrganizationsView outView = (OrganizationsView) orgService.getOrganizationInfo(organizationId).data;
        Assert.assertEquals(organizationId, outView.id);
    }

    @Test
    public void getOrganizationsByParameters() {
        String organizationNameFromDB = "Second organization";
        OrganizationsView inView = new OrganizationsView();
        inView.name = organizationNameFromDB;
        List listOfaOrganization = (List) orgService.getOrganizationsByFilter(inView).data;
        Assert.assertEquals(1, listOfaOrganization.size());
        OrganizationsModel organization = (OrganizationsModel) listOfaOrganization.get(0);
        Assert.assertEquals(organizationNameFromDB, organization.getName());
    }

    @Test
    @Transactional
    public void updateOrganization() {
        OrganizationsView outView = (OrganizationsView) orgService.getOrganizationInfo(2).data;
        OrganizationsView inView = outView;
        String name = Utils.getUniqueNameForTest();
        inView.name = name;
        String result = orgService.updateOrganization(inView).result;
        Assert.assertEquals(success, result);
    }

    @Test
    @Transactional
    public void deleteOrganization() {
        long organizationId = 2;
        OrganizationsView inView = new OrganizationsView();
        inView.id = organizationId;
        String result = orgService.deleteOrganization(inView).result;
        Assert.assertEquals(success, result);
    }
}
