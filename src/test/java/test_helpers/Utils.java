package test_helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    public static String getUniqueNameForTest() {
        DateFormat format = new SimpleDateFormat("dd.MM HH:mm:ss:SS");
        return "test-" + format.format(new Date());
    }

}
