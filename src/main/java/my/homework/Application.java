package my.homework;


import my.homework.dictionaries.DictController;
import my.homework.dictionaries.DictService;
import my.homework.dictionaries.DictionaryDao;
import my.homework.employees.EmployeeController;
import my.homework.employees.EmployeeService;
import my.homework.employees.EmployeesDao;
import my.homework.offices.OfficeController;
import my.homework.offices.OfficeDao;
import my.homework.offices.OfficeService;
import my.homework.organizations.OrganizationsController;
import my.homework.organizations.OrganizationsDao;
import my.homework.organizations.OrganizationsService;
import my.homework.users.UserController;
import my.homework.users.UsersDao;
import my.homework.users.UsersService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ImportResource;

import java.sql.SQLException;

@ImportResource("spring_mvc_config.xml")
@SpringBootApplication
@ComponentScan(basePackageClasses = {
        UserController.class, UsersService.class, UsersDao.class,
        OrganizationsController.class, OrganizationsService.class, OrganizationsDao.class,
        OfficeController.class, OfficeService.class, OfficeDao.class,
        EmployeeController.class, EmployeeService.class, EmployeesDao.class,
        DictController.class, DictService.class, DictionaryDao.class
})

public class Application {

    public static void main(String[] args) throws SQLException {
        //Server.createTcpServer("-tcpPort", "9092", "-tcpAllowOthers").start();
        SpringApplication app = new SpringApplication(Application.class);
        app.run(args);
    }
}