package my.homework.employees;

import my.homework.dictionaries.CountriesModel;
import my.homework.offices.OfficesModel;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;

/*
Работники организации
*/
@Entity
@Table(name = "employees")
public class EmployeeModel {

    @Version
    private Integer version;

    /*
    Первичный ключ
    */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    /*
    Имя
    */
    @Basic(optional = false)
    @Column(name = "first_name")
    private String firstName;

    /*
    Фамилия
    */
    @Basic(optional = false)
    @Column(name = "last_name")
    private String lastName;

    /*
    Отчество
    */
    @Basic(optional = false)
    @Column(name = "middle_name")
    private String middleName;

    /*
    Телефон
    */
    @Basic(optional = false)
    @Column(name = "phone")
    private int phone;

    /*
    Внешний ключ - id офиса
    */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "office_id")
    private OfficesModel office;

    /*
    Должность
    */
    @Basic(optional = false)
    @Column(name = "position")
    private String position;

    /*
        Внешний ключ - id записи из таблицы,
         в которой документы привязаны к работнику
        */
    @OneToOne(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "doc_number")
    private DocsOfEmployeesModel document;

    /*
    Внешний ключ - код страны
    */
    @OneToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "citizen_ship_code")
    private CountriesModel citizenShipCode;

    /*
    Статус работника
    */
    @Basic(optional = false)
    @Column(name = "is_identified")
    private boolean isIdentified;

    public EmployeeModel() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public OfficesModel getOffice() {
        return office;
    }

    public void setOffice(OfficesModel office) {
        this.office = office;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    public DocsOfEmployeesModel getDocument() {
        return document;
    }

    public void setDocument(DocsOfEmployeesModel document) {
        this.document = document;
    }

    public CountriesModel getCitizenShipCode() {
        return citizenShipCode;
    }

    public void setCitizenShipCode(CountriesModel citizenShipCode) {
        this.citizenShipCode = citizenShipCode;
    }

    public boolean isIdentified() {
        return isIdentified;
    }

    public void setIdentified(boolean identified) {
        isIdentified = identified;
    }
}
