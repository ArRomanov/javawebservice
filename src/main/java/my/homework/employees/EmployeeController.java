package my.homework.employees;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api/employee")
public class EmployeeController {

    private EmployeeService employeeService;

    @Autowired
    public EmployeeController(EmployeeService employeeService) {
        this.employeeService = employeeService;
    }

    @RequestMapping(value = "/list", method = POST)
    public SystemResponseView getEmployees(@RequestBody EmployeeView employeeView) {
        return employeeService.getListOfEmployees(employeeView);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public SystemResponseView getEmployeeById(@PathVariable("id") long id) {
        return employeeService.getEmployeeById(id);
    }

    @RequestMapping(value = "/update", method = POST)
    public SystemResponseView updateEmployeeInfo(@RequestBody EmployeeView employeeView) {
        return employeeService.updateEmployee(employeeView);
    }

    @RequestMapping(value = "/delete", method = POST)
    public SystemResponseView deleteEmployee(@RequestBody EmployeeView employeeView) {
        return employeeService.deleteOfEmployee(employeeView.id);
    }

    @RequestMapping(value = "/save", method = POST)
    public SystemResponseView createNewEmployee(@RequestBody EmployeeView employeeView) {
        try {
            return employeeService.addNewEmployee(employeeView);
        } catch (DataIntegrityViolationException ex) {
            return new SystemResponseView("Error! The required fields are not filled");
        }
    }
}
