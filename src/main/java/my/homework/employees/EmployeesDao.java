package my.homework.employees;

import my.homework.dictionaries.CountriesModel;
import my.homework.dictionaries.DocumentsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class EmployeesDao {

    private final EntityManager em;

    @Autowired
    public EmployeesDao(EntityManager em) {
        this.em = em;
    }

    public List<EmployeeModel> getListOfEmployees(EmployeeModel employeeModel, long officeId, long document, int citizenShipCode) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<EmployeeModel> criteria = builder.createQuery(EmployeeModel.class);
        Root rootEmployee = criteria.from(EmployeeModel.class);
        List<Predicate> predicates = new ArrayList<>();

        Join joinOffice = rootEmployee.join("office");
        predicates.add(builder.equal(joinOffice.get("id"), officeId));
        if (document != 0) {
            Join joinDocument = rootEmployee.join("docNumber");
            predicates.add(builder.equal(joinDocument.get("id"), document));
        }
        if (citizenShipCode != 0) {
            Join joinCountry = rootEmployee.join("citizenShipCode");
            predicates.add(builder.equal(joinCountry.get("code"), citizenShipCode));
        }
        if (employeeModel.getFirstName() != null) {
            predicates.add(builder.equal(rootEmployee.get("firstName"), employeeModel.getFirstName()));
        }
        if (employeeModel.getLastName() != null) {
            predicates.add(builder.equal(rootEmployee.get("lastName"), employeeModel.getLastName()));
        }
        if (employeeModel.getMiddleName() != null) {
            predicates.add(builder.equal(rootEmployee.get("middleName"), employeeModel.getMiddleName()));
        }
        if (employeeModel.getPosition() != null) {
            predicates.add(builder.equal(rootEmployee.get("position"), employeeModel.getPosition()));
        }
        criteria.select(rootEmployee).where(builder.and(predicates.toArray(new Predicate[]{})));
        return em.createQuery(criteria).getResultList();
    }

    public EmployeeModel getEmployeeById(long id) {
        return em.find(EmployeeModel.class, id);
    }

    public void deleteEmployeeById(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete<EmployeeModel> delete = criteriaBuilder.createCriteriaDelete(EmployeeModel.class);
        Root<EmployeeModel> rootEmployee = delete.from(EmployeeModel.class);
        delete.where(criteriaBuilder.equal(rootEmployee.get("id"), id));
        em.createQuery(delete).executeUpdate();
    }

    public void updateEmployee(EmployeeModel employeeModel, String docName, DocsOfEmployeesModel docOfEmployees) {
        EmployeeModel oldEmployeeInfo = em.find(EmployeeModel.class, employeeModel.getId());
        oldEmployeeInfo.setId(employeeModel.getId());
        oldEmployeeInfo.setFirstName(employeeModel.getFirstName());
        oldEmployeeInfo.setLastName(employeeModel.getLastName());
        oldEmployeeInfo.setMiddleName(employeeModel.getMiddleName());
        oldEmployeeInfo.setPosition(employeeModel.getPosition());
        oldEmployeeInfo.setPhone(employeeModel.getPhone());
        oldEmployeeInfo.setIdentified(employeeModel.isIdentified());
        oldEmployeeInfo.setCitizenShipCode(employeeModel.getCitizenShipCode());
        if (docName != null) {
            CriteriaBuilder builder = em.getCriteriaBuilder();
            CriteriaQuery<DocumentsModel> criteria = builder.createQuery(DocumentsModel.class);
            criteria.where(builder.equal(criteria.from(DocumentsModel.class).get("name"), docName));
            DocumentsModel docCodeModel = em.createQuery(criteria).getSingleResult();

            DocsOfEmployeesModel oldDoc = oldEmployeeInfo.getDocument();
            oldDoc.setDocCode(docCodeModel);
            oldDoc.setDocDate(docOfEmployees.getDocDate());
            oldDoc.setDocNumber(docOfEmployees.getDocNumber());
        }
        em.merge(oldEmployeeInfo);
    }

    public void addNewEmployee(EmployeeModel newEmployee, DocumentsModel docCodeModel, CountriesModel countryModel) {
        DocumentsModel typeOfDocument = em.find(DocumentsModel.class, docCodeModel.getCode());
        if (typeOfDocument != null) {
            newEmployee.getDocument().setDocCode(typeOfDocument);
        }
        CountriesModel country = em.find(CountriesModel.class, countryModel.getCode());
        if (country != null) {
            newEmployee.setCitizenShipCode(country);
        }
        em.persist(newEmployee);
    }
}