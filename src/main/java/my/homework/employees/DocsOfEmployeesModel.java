package my.homework.employees;

import my.homework.dictionaries.DocumentsModel;

import javax.persistence.*;

/*
Документы работников
*/
@Entity
@Table(name = "docs_of_employees")
public class DocsOfEmployeesModel {

    @Version
    private Integer version;

    /*
    Первичный ключ
    */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    /*
        Внешний ключ для таблицы с типами документов
        */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "doc_code")
    private DocumentsModel docCode;

    /*
    серия/номер документа
    */
    @Basic(optional = false)
    @Column(name = "doc_number")
    private long docNumber;

    /*
    Дата выдачи документа
    */
    @Basic(optional = false)
    @Column(name = "doc_date")
    private String docDate;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public DocumentsModel getDocCode() {
        return docCode;
    }

    public void setDocCode(DocumentsModel docCode) {
        this.docCode = docCode;
    }

    public long getDocNumber() {
        return docNumber;
    }

    public void setDocNumber(long docNumber) {
        this.docNumber = docNumber;
    }

    public String getDocDate() {
        return docDate;
    }

    public void setDocDate(String docDate) {
        this.docDate = docDate;
    }
}
