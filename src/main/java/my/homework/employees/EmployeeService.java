package my.homework.employees;

import my.homework.SystemResponseView;
import my.homework.dictionaries.CountriesModel;
import my.homework.dictionaries.DocumentsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class EmployeeService {

    private final EmployeesDao dao;

    @Autowired
    public EmployeeService(EmployeesDao dao) {
        this.dao = dao;
    }

    @Transactional
    public SystemResponseView getListOfEmployees(EmployeeView employeeView) {
        EmployeeModel employeeModel = new EmployeeModel();
        employeeModel.setFirstName(employeeView.firstName);
        employeeModel.setLastName(employeeView.lastName);
        employeeModel.setMiddleName(employeeView.middleName);
        employeeModel.setPosition(employeeView.position);
        List<EmployeeModel> employeeList = dao.getListOfEmployees(
                employeeModel,
                employeeView.office,
                employeeView.docNumber,
                employeeView.citizenShipCode);
        return new SystemResponseView(employeeList);
    }

    @Transactional
    public SystemResponseView getEmployeeById(long id) {
        EmployeeModel employeeModel = dao.getEmployeeById(id);
        EmployeeBuilder employeeBuilder = new EmployeeBuilder();
        try {
            employeeBuilder
                    .setId(employeeModel.getId())
                    .setFirstName(employeeModel.getFirstName())
                    .setLastName(employeeModel.getLastName())
                    .setMiddleName(employeeModel.getMiddleName())
                    .setPosition(employeeModel.getPosition())
                    .setPhone(employeeModel.getPhone())
                    .setDocName(employeeModel.getDocument().getDocCode().getName())
                    .setDocNumber(employeeModel.getDocument().getDocNumber())
                    .setDocDate(employeeModel.getDocument().getDocDate())
                    .setCitizenShipCode(employeeModel.getCitizenShipCode().getCode())
                    .setCitizenShipName(employeeModel.getCitizenShipCode().getName())
                    .setIdentified(employeeModel.isIdentified());
            return new SystemResponseView(new EmployeeView(employeeBuilder));
        } catch (NullPointerException ex) {
            return new SystemResponseView("Employee not found");
        }
    }

    @Transactional
    public SystemResponseView deleteOfEmployee(long id) {
        dao.deleteEmployeeById(id);
        return new SystemResponseView();
    }

    @Transactional
    public SystemResponseView updateEmployee(EmployeeView employeeView) {
        DocsOfEmployeesModel document = new DocsOfEmployeesModel();
        document.setDocNumber(employeeView.docNumber);
        document.setDocDate(employeeView.docDate);
        EmployeeModel employeeModel = new EmployeeModel();
        employeeModel.setId(employeeView.id);
        employeeModel.setFirstName(employeeView.firstName);
        employeeModel.setLastName(employeeView.lastName);
        employeeModel.setMiddleName(employeeView.middleName);
        employeeModel.setPosition(employeeView.position);
        employeeModel.setPhone(employeeView.phone);
        employeeModel.setIdentified(employeeView.isIdentified);
        if (employeeView.citizenShipCode != 0) {
            CountriesModel country = new CountriesModel();
            country.setName(employeeView.citizenShipName);
            country.setCode(employeeView.citizenShipCode);
            employeeModel.setCitizenShipCode(country);
        }
        try {
            dao.updateEmployee(employeeModel, employeeView.docName, document);
            return new SystemResponseView();
        } catch (EmptyResultDataAccessException ex) {
            return new SystemResponseView("Error! Wrong type of document");
        } catch (NullPointerException ex) {
            return new SystemResponseView("Error! Object with such ID not found");
        }
    }

    @Transactional
    public SystemResponseView addNewEmployee(EmployeeView employeeView) {
        DocumentsModel docCode = new DocumentsModel();
        docCode.setCode(employeeView.docCode);
        docCode.setName(employeeView.docName);

        CountriesModel country = new CountriesModel();
        country.setCode(employeeView.citizenShipCode);
        country.setName(employeeView.citizenShipName);

        DocsOfEmployeesModel document = new DocsOfEmployeesModel();
        EmployeeModel newEmployee = new EmployeeModel();

        if (employeeView.docNumber != 0
                && employeeView.docDate != null) {
            document.setDocNumber(employeeView.docNumber);
            document.setDocDate(employeeView.docDate);
            newEmployee.setDocument(document);
        }

        newEmployee.setFirstName(employeeView.firstName);
        newEmployee.setLastName(employeeView.lastName);
        newEmployee.setMiddleName(employeeView.middleName);
        newEmployee.setPosition(employeeView.position);
        newEmployee.setPhone(employeeView.phone);
        newEmployee.setIdentified(employeeView.isIdentified);
        dao.addNewEmployee(newEmployee, docCode, country);
        return new SystemResponseView();
    }
}
