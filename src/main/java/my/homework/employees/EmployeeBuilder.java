package my.homework.employees;

public class EmployeeBuilder {
    private long id;
    private long officeId;
    private String firstName;
    private String lastName;
    private String middleName;
    private int phone;
    private String position;
    private long docNumber;
    private String docName;
    private String docDate;
    private int citizenShipCode;
    private String citizenShipName;
    private boolean isIdentified;

    public long getId() {
        return id;
    }

    public EmployeeBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public long getOfficeId() {
        return officeId;
    }

    public EmployeeBuilder setOfficeId(long officeId) {
        this.officeId = officeId;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public EmployeeBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public EmployeeBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getMiddleName() {
        return middleName;
    }

    public EmployeeBuilder setMiddleName(String middleName) {
        this.middleName = middleName;
        return this;
    }

    public int getPhone() {
        return phone;
    }

    public EmployeeBuilder setPhone(int phone) {
        this.phone = phone;
        return this;
    }

    public String getPosition() {
        return position;
    }

    public EmployeeBuilder setPosition(String position) {
        this.position = position;
        return this;
    }

    public long getDocNumber() {
        return docNumber;
    }

    public EmployeeBuilder setDocNumber(long docNumber) {
        this.docNumber = docNumber;
        return this;
    }

    public int getCitizenShipCode() {
        return citizenShipCode;
    }

    public EmployeeBuilder setCitizenShipCode(int citizenShipCode) {
        this.citizenShipCode = citizenShipCode;
        return this;
    }

    public boolean isIdentified() {
        return isIdentified;
    }

    public EmployeeBuilder setIdentified(boolean identified) {
        this.isIdentified = identified;
        return this;
    }

    public String getDocName() {
        return docName;
    }

    public EmployeeBuilder setDocName(String docName) {
        this.docName = docName;
        return this;
    }

    public String getDocDate() {
        return docDate;
    }

    public EmployeeBuilder setDocDate(String docDate) {
        this.docDate = docDate;
        return this;
    }


    public String getCitizenShipName() {
        return citizenShipName;
    }

    public EmployeeBuilder setCitizenShipName(String citizenShipName) {
        this.citizenShipName = citizenShipName;
        return this;
    }
}
