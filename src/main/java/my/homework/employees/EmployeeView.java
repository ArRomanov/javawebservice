package my.homework.employees;

public class EmployeeView {

    public long id;
    public long office;
    public String firstName;
    public String lastName;
    public String middleName;
    public int phone;
    public String position;
    public int docCode;
    public long docNumber;
    public String docName;
    public String docDate;
    public int citizenShipCode;
    public String citizenShipName;
    public boolean isIdentified;

    public EmployeeView() {
    }

    public EmployeeView(EmployeeBuilder employee) {
        this.id = employee.getId();
        this.firstName = employee.getFirstName();
        this.lastName = employee.getLastName();
        this.middleName = employee.getMiddleName();
        this.position = employee.getPosition();
        this.phone = employee.getPhone();
        this.docNumber = employee.getDocNumber();
        this.citizenShipCode = employee.getCitizenShipCode();
        this.citizenShipName = employee.getCitizenShipName();
        this.isIdentified = employee.isIdentified();
        this.docName = employee.getDocName();
        this.docDate = employee.getDocDate();
    }
}
