package my.homework.dictionaries;

public class DocsView {

    public String name;
    public int code;

    public DocsView() {
    }

    public DocsView(String name, int code) {
        this.name = name;
        this.code = code;
    }
}
