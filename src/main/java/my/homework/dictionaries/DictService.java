package my.homework.dictionaries;

import my.homework.SystemResponseView;
import my.homework.employees.EmployeesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class DictService {

    private final DictionaryDao dao;

    @Autowired
    public DictService(DictionaryDao dao) {
        this.dao = dao;
    }

    @Transactional
    public SystemResponseView getAllTypesOfDocs() {
        return new SystemResponseView(dao.getAllTypeOfDocs());
    }

    @Transactional
    public SystemResponseView getAllCodesOfCountries() {
        return new SystemResponseView(dao.getAllCodesOfCountries());
    }
}
