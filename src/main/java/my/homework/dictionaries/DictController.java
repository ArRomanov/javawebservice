package my.homework.dictionaries;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

@RestController
@RequestMapping(value = "/api")
public class DictController {

    private DictService dictService;

    @Autowired
    public DictController(DictService dictService) {
        this.dictService = dictService;
    }

    @RequestMapping(value = "/docs", method = GET)
    public SystemResponseView getDocsDictionary() {
        return dictService.getAllTypesOfDocs();
    }

    @RequestMapping(value = "/countries", method = GET)
    public SystemResponseView getCountryDictionary() {
        return dictService.getAllCodesOfCountries();
    }
}
