package my.homework.dictionaries;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = "getAllCodesOfCountries",
                query = "from CountriesModel")
})

@Entity
@Table(name = "countries_list")
public class CountriesModel {

    /*
    Первичный ключ - код страны
    */
    @Id
    @Column(name = "code")
    private int code;

    /*
    Страна (имя)
    */
    @Column(name = "name")
    private String name;

    public CountriesModel() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
