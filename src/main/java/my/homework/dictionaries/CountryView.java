package my.homework.dictionaries;

public class CountryView {

    public String name;
    public int code;

    public CountryView() {
    }

    public CountryView(String name, int code) {
        this.name = name;
        this.code = code;
    }
}
