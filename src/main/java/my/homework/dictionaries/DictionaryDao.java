package my.homework.dictionaries;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Repository
public class DictionaryDao {

    private final EntityManager em;

    @Autowired
    public DictionaryDao(EntityManager em) {
        this.em = em;
    }

    public List<DocumentsModel> getAllTypeOfDocs() {
        Query query = em.createNamedQuery("getAllTypesOfDocs");
        return query.getResultList();
    }

    public List<CountriesModel> getAllCodesOfCountries() {
        Query query = em.createNamedQuery("getAllCodesOfCountries");
        return query.getResultList();
    }
}
