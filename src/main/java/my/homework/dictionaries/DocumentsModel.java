package my.homework.dictionaries;

import javax.persistence.*;

@NamedQueries({
        @NamedQuery(
                name = "getAllTypesOfDocs",
                query = "from DocumentsModel")
})

@Entity
@Table(name = "types_of_documents")
public class DocumentsModel {

    /*
    Первичный ключ
    */
    @Id
    @Column(name = "code")
    private int code;

    /*
    Тип документа (название)
    */
    @Column(name = "name")
    private String name;

    public DocumentsModel() {

    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
