package my.homework.organizations;

import javax.persistence.*;

/*
    Организации
    */
@Entity
@Table(name = "organizations")
public class OrganizationsModel {

    @Version
    private Integer version;

    /*
    Первичный ключ
    */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    /*
    Название
    */
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    /*
    Полное название (юридическое)
    */
    @Basic(optional = false)
    @Column(name = "full_name")
    private String fullName;

    /*
    ИНН
    */
    @Basic(optional = false)
    @Column(name = "inn")
    private long inn;

    /*
    КПП
    */
    @Basic(optional = false)
    @Column(name = "kpp")
    private long kpp;

    /*
    Адрес
    */
    @Basic(optional = false)
    @Column(name = "address")
    private String address;

    /*
    Телефон
    */
    @Basic(optional = false)
    @Column(name = "phone")
    private long phone;

    /*
    Статус организации
    */
    @Basic(optional = false)
    @Column(name = "is_active")
    private boolean isActive;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public long getInn() {
        return inn;
    }

    public void setInn(long inn) {
        this.inn = inn;
    }

    public long getKpp() {
        return kpp;
    }

    public void setKpp(long kpp) {
        this.kpp = kpp;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public long getPhone() {
        return phone;
    }

    public void setPhone(long phone) {
        this.phone = phone;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

}
