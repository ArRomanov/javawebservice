package my.homework.organizations;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
public class OrganizationsService {

    private final OrganizationsDao dao;

    @Autowired
    public OrganizationsService(OrganizationsDao dao) {
        this.dao = dao;
    }

    @Transactional
    public SystemResponseView createNewOrganization(OrganizationsView organizationsView) {
        OrganizationsModel organizationsModel = new OrganizationsModel();
        organizationsModel.setName(organizationsView.name);
        organizationsModel.setFullName(organizationsView.fullName);
        organizationsModel.setInn(organizationsView.inn);
        organizationsModel.setKpp(organizationsView.kpp);
        organizationsModel.setAddress(organizationsView.address);
        organizationsModel.setPhone(organizationsView.phone);
        organizationsModel.setActive(organizationsView.isActive);
        try {
            dao.createOrganization(organizationsModel);
            return new SystemResponseView();
        } catch (Exception ex) {
            return new SystemResponseView("Save organization failed");
        }
    }

    @Transactional
    public SystemResponseView getOrganizationInfo(long id) {
        OrganizationsModel organization = dao.getOrganization(id);
        OrganizationBuilder orgBuilder = new OrganizationBuilder();
        OrganizationsView orgView;
        try {
            orgBuilder
                    .setActive(organization.isActive())
                    .setAddress(organization.getAddress())
                    .setFullName(organization.getFullName())
                    .setId(organization.getId())
                    .setName(organization.getName())
                    .setInn(organization.getInn())
                    .setKpp(organization.getKpp())
                    .setPhone(organization.getPhone());
            orgView = new OrganizationsView(orgBuilder);
            return new SystemResponseView(orgView);
        } catch (NullPointerException ex) {
            return new SystemResponseView("Organization not found");
        }
    }

    @Transactional
    public SystemResponseView getOrganizationsByFilter(OrganizationsView organizationView) {
        OrganizationsModel model = new OrganizationsModel();
        model.setName(organizationView.name);
        model.setInn(organizationView.inn);
        model.setActive(organizationView.isActive);
        List<OrganizationsModel> organizationList = dao.getOrganizationsByFilter(model);
        return new SystemResponseView(organizationList);
    }

    @Transactional
    public SystemResponseView updateOrganization(OrganizationsView organizationView) {
        try {
            dao.updateOrganization(organizationView);
            return new SystemResponseView();
        } catch (Exception ex) {
            return new SystemResponseView("Updating of organization failed");
        }
    }

    @Transactional
    public SystemResponseView deleteOrganization(OrganizationsView organizationView) {
        try {
            dao.deleteOrganization(organizationView.id);
            return new SystemResponseView();
        } catch (Exception ex) {
            return new SystemResponseView("Deleting of organization failed");
        }
    }
}
