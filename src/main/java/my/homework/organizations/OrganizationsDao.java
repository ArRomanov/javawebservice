package my.homework.organizations;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.*;

@Repository
public class OrganizationsDao {

    private final EntityManager em;

    @Autowired
    public OrganizationsDao(EntityManager em) {
        this.em = em;
    }

    public void createOrganization(OrganizationsModel organizationsModel) {
        em.persist(organizationsModel);
    }

    public OrganizationsModel getOrganization(long id) {
        return em.find(OrganizationsModel.class, id);
    }

    public List<OrganizationsModel> getOrganizationsByFilter(OrganizationsModel model) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<OrganizationsModel> criteria = builder.createQuery(OrganizationsModel.class);
        Root<OrganizationsModel> rootModel = criteria.from(OrganizationsModel.class);
        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(rootModel.get("name"), model.getName()));
        if (model.getInn() != 0) {
            predicates.add(builder.equal(rootModel.get("inn"), model.getInn()));
        }
        if (model.isActive()) {
            predicates.add(builder.equal(rootModel.get("isActive"), model.isActive()));
        }
        criteria.select(rootModel).where(builder.and(predicates.toArray(new Predicate[]{})));
        return em.createQuery(criteria).getResultList();
    }

    public void updateOrganization(OrganizationsView view) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaUpdate<OrganizationsModel> criteriaUpdate = builder.createCriteriaUpdate(OrganizationsModel.class);

        Root<OrganizationsModel> rootOrganization = criteriaUpdate.from(OrganizationsModel.class);
        criteriaUpdate
                .set("name", view.name)
                .set("fullName", view.fullName)
                .set("inn", view.inn)
                .set("kpp", view.kpp)
                .set("address", view.address)
                .set("phone", view.phone)
                .set("isActive", true);
        criteriaUpdate.where(builder.equal(rootOrganization.get("id"), view.id));
        em.createQuery(criteriaUpdate).executeUpdate();
    }

    public void deleteOrganization(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete<OrganizationsModel> criteriaDelete = criteriaBuilder.createCriteriaDelete(OrganizationsModel.class);
        Root<OrganizationsModel> rootOrganization = criteriaDelete.from(OrganizationsModel.class);
        criteriaDelete.where(criteriaBuilder.equal(rootOrganization.get("id"), id));
        em.createQuery(criteriaDelete).executeUpdate();
    }
}
