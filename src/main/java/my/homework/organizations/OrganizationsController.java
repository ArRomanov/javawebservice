package my.homework.organizations;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api/organization")
public class OrganizationsController {

    private OrganizationsService organizationsService;

    @Autowired
    public OrganizationsController(OrganizationsService organizationsService) {
        this.organizationsService = organizationsService;
    }

    @RequestMapping(value = "/save", method = {POST})
    public SystemResponseView registerNewOrganization(@RequestBody OrganizationsView organizationView) {
        return organizationsService.createNewOrganization(organizationView);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public SystemResponseView getOrganizationInfo(@PathVariable("id") long id) {
        return organizationsService.getOrganizationInfo(id);
    }

    @RequestMapping(value = "/list", method = POST)
    public SystemResponseView getOrganizationByFilter(@RequestBody OrganizationsView organizationView) {
        return organizationsService.getOrganizationsByFilter(organizationView);
    }

    @RequestMapping(value = "/update", method = POST)
    public SystemResponseView updateOrganizationInfo(@RequestBody OrganizationsView organizationView) {
        return organizationsService.updateOrganization(organizationView);
    }

    @RequestMapping(value = "/delete", method = POST)
    public SystemResponseView deleteOrganization(@RequestBody OrganizationsView organizationView) {
        return organizationsService.deleteOrganization(organizationView);
    }
}
