package my.homework.organizations;

public class OrganizationBuilder {
    private String name;
    private long inn;
    private boolean isActive;
    private long id;
    private String fullName;
    private long kpp;
    private String address;
    private long phone;

    public String getName() {
        return name;
    }

    public OrganizationBuilder setName(String name) {
        this.name = name;
        return this;
    }

    public long getInn() {
        return inn;
    }

    public OrganizationBuilder setInn(long inn) {
        this.inn = inn;
        return this;
    }

    public boolean isActive() {
        return isActive;
    }

    public OrganizationBuilder setActive(boolean active) {
        isActive = active;
        return this;
    }

    public long getId() {
        return id;
    }

    public OrganizationBuilder setId(long id) {
        this.id = id;
        return this;
    }

    public String getFullName() {
        return fullName;
    }

    public OrganizationBuilder setFullName(String fullName) {
        this.fullName = fullName;
        return this;
    }

    public long getKpp() {
        return kpp;
    }

    public OrganizationBuilder setKpp(long kpp) {
        this.kpp = kpp;
        return this;
    }

    public String getAddress() {
        return address;
    }

    public OrganizationBuilder setAddress(String address) {
        this.address = address;
        return this;
    }

    public long getPhone() {
        return phone;
    }

    public OrganizationBuilder setPhone(long phone) {
        this.phone = phone;
        return this;
    }
}
