package my.homework.organizations;

public class OrganizationsView {

    public String name;
    public long inn;
    public boolean isActive;
    public long id;
    public String fullName;
    public long kpp;
    public String address;
    public long phone;

    public OrganizationsView() {

    }

    public OrganizationsView(OrganizationBuilder builder) {
        this.id = builder.getId();
        this.name = builder.getName();
        this.fullName = builder.getFullName();
        this.inn = builder.getInn();
        this.kpp = builder.getKpp();
        this.address = builder.getAddress();
        this.phone = builder.getPhone();
        this.isActive = builder.isActive();
    }
}
