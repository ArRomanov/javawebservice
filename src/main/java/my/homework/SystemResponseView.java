package my.homework;

public class SystemResponseView {

    public String errorMessage;
    public String result;
    public Object data;

    public SystemResponseView() {
        result = "success";
    }

    public SystemResponseView(Object data) {
        result = "success";
        this.data = data;
    }

    public SystemResponseView(String errorMessage) {
        result = "error";
        this.errorMessage = errorMessage;
    }
}
