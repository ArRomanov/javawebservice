package my.homework.offices;

public class OfficeView {

    public long id;
    public long organizationId;
    public String name;
    public String address;
    public int phone;
    public boolean isActive;

    public OfficeView() {
    }

    public OfficeView(long id, String name) {
        this.id = id;
        this.name = name;
        this.isActive = true;
    }

    public OfficeView(long id, String name, String address, int phone) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.phone = phone;
        this.isActive = true;
    }

}
