package my.homework.offices;

import my.homework.organizations.OrganizationsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class OfficeDao {

    private final EntityManager em;

    @Autowired
    public OfficeDao(EntityManager em) {
        this.em = em;
    }

    public List<OfficesModel> getListOfOffices(OfficesModel officesModel) {
        OrganizationsModel organizations = em.find(OrganizationsModel.class, officesModel.getOrganization().getId());
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<OfficesModel> criteria = builder.createQuery(OfficesModel.class);
        Root<OfficesModel> rootOffice = criteria.from(OfficesModel.class);
        List<Predicate> predicates = new ArrayList<>();

        predicates.add(builder
                .equal(rootOffice.get("organization"), organizations));
        if (officesModel.getName() != null) {
            predicates.add(builder.equal(rootOffice.get("name"), officesModel.getName()));
        }
        if (officesModel.isActive()) {
            predicates.add(builder.equal(rootOffice.get("isActive"), officesModel.isActive()));
        }
        if (officesModel.getPhone() != 0) {
            predicates.add(builder.equal(rootOffice.get("phone"), officesModel.getPhone()));
        }

        criteria.select(rootOffice).where(builder.and(predicates.toArray(new Predicate[]{})));
        return em.createQuery(criteria).getResultList();
    }

    public OfficesModel getOfficeById(long id) {
        return em.find(OfficesModel.class, id);
    }

    public void updateOfficeInfo(OfficesModel officeModel) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaUpdate<OfficesModel> update = criteriaBuilder.createCriteriaUpdate(OfficesModel.class);

        Root<OfficesModel> rootOffice = update.from(OfficesModel.class);
        update
                .set("name", officeModel.getName())
                .set("address", officeModel.getAddress())
                .set("phone", officeModel.getPhone())
                .set("isActive", officeModel.isActive());
        update.where(criteriaBuilder.equal(rootOffice.get("id"), officeModel.getId()));
        em.createQuery(update).executeUpdate();
    }

    public void deleteOfficeById(long id) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaDelete<OfficesModel> delete = criteriaBuilder.createCriteriaDelete(OfficesModel.class);
        Root<OfficesModel> rootOffice = delete.from(OfficesModel.class);
        delete.where(criteriaBuilder.equal(rootOffice.get("id"), id));
        em.createQuery(delete).executeUpdate();
    }

    public void createNewOffice(OfficesModel officeModel, long organizationId) {
        OrganizationsModel orgModel = em.find(OrganizationsModel.class, organizationId);
        officeModel.setOrganization(orgModel);
        em.persist(officeModel);
    }
}
