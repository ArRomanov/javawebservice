package my.homework.offices;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api/office")
public class OfficeController {

    private OfficeService officeService;

    @Autowired
    public OfficeController(OfficeService officeService) {
        this.officeService = officeService;
    }

    @RequestMapping(value = "/list/{orgId}", method = POST)
    public SystemResponseView getOfficeByOrgId(@PathVariable("orgId") long orgId, @RequestBody OfficeView officeView) {
        return officeService.getListOfOffices(orgId, officeView);
    }

    @RequestMapping(value = "/{id}", method = GET)
    public SystemResponseView getOfficeById(@PathVariable("id") long id) {
        return officeService.getOfficeById(id);
    }

    @RequestMapping(value = "/update", method = POST)
    public SystemResponseView updateOfficeInfo(@RequestBody OfficeView officeView) {
        return officeService.updateOfficeInfo(officeView);
    }

    @RequestMapping(value = "/delete", method = POST)
    public SystemResponseView deleteOffice(@RequestBody OfficeView officeView) {
        return officeService.deleteOffice(officeView.id);
    }

    @RequestMapping(value = "/save", method = POST)
    public SystemResponseView createNewOffice(@RequestBody OfficeView officeView) {
        try {
            return officeService.createNewOffice(officeView);
        } catch (DataIntegrityViolationException ex) {
            return new SystemResponseView("Save office failed!");
        }
    }
}
