package my.homework.offices;

import my.homework.organizations.OrganizationsModel;

import javax.persistence.*;

/*
    Офисы организаций
    */
@Entity
@Table(name = "offices")
public class OfficesModel {

    @Version
    private Integer version;

    /*
    Первичный ключ
    */
    @Id
    @GeneratedValue
    @Column(name = "id")
    private long id;

    /*
    Именной идентификатор офиса
    */
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    /*
    Телефон
    */
    @Basic(optional = false)
    @Column(name = "phone")
    private int phone;

    /*
    Адрес
    */
    @Basic(optional = false)
    @Column(name = "address")
    private String address;

    /*
    Внешний ключ - id организации
    */
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "organization_id")
    private OrganizationsModel organization;

    /*
    Статус офиса
    */
    @Basic(optional = false)
    @Column(name = "is_active")
    private boolean isActive;

    public OfficesModel() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPhone() {
        return phone;
    }

    public void setPhone(int phone) {
        this.phone = phone;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public OrganizationsModel getOrganization() {
        return organization;
    }

    public void setOrganization(OrganizationsModel organization) {
        this.organization = organization;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean isActive) {
        this.isActive = isActive;
    }
}
