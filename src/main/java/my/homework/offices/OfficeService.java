package my.homework.offices;

import my.homework.SystemResponseView;
import my.homework.organizations.OrganizationsModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OfficeService {

    private final OfficeDao dao;

    @Autowired
    public OfficeService(OfficeDao dao) {
        this.dao = dao;
    }

    @Transactional
    public SystemResponseView getListOfOffices(long orgId, OfficeView officeView) {
        OfficesModel officesModel = new OfficesModel();
        officesModel.setName(officeView.name);
        officesModel.setPhone(officeView.phone);
        officesModel.setActive(officeView.isActive);
        OrganizationsModel organization = new OrganizationsModel();
        organization.setId(orgId);
        officesModel.setOrganization(organization);
        List<OfficesModel> officesList = dao.getListOfOffices(officesModel);
        return new SystemResponseView(officesList);
    }

    @Transactional
    public SystemResponseView getOfficeById(long id) {
        OfficesModel office = dao.getOfficeById(id);
        OfficeView officeView = new OfficeView();
        try {
            officeView.id = office.getId();
            officeView.name = office.getName();
            officeView.address = office.getAddress();
            officeView.phone = office.getPhone();
            return new SystemResponseView(officeView);
        } catch (NullPointerException ex) {
            return new SystemResponseView("Office not found");
        }
    }

    @Transactional
    public SystemResponseView updateOfficeInfo(OfficeView officeView) {
        OfficesModel officeModel = new OfficesModel();
        officeModel.setId(officeView.id);
        officeModel.setName(officeView.name);
        officeModel.setAddress(officeView.address);
        officeModel.setPhone(officeView.phone);
        officeModel.setActive(officeView.isActive);
        dao.updateOfficeInfo(officeModel);
        return new SystemResponseView();
    }

    @Transactional
    public SystemResponseView deleteOffice(long id) {
        try {
            dao.deleteOfficeById(id);
            return new SystemResponseView();
        } catch (DataIntegrityViolationException ex) {
            return new SystemResponseView("Something went wrong...\n " +
                    "Probably it has employees.\n" +
                    "Try delete employees from this office and delete it.");
        }
    }

    @Transactional
    public SystemResponseView createNewOffice(OfficeView officeView) {
        OfficesModel officeModel = new OfficesModel();
        officeModel.setName(officeView.name);
        officeModel.setAddress(officeView.address);
        officeModel.setPhone(officeView.phone);
        officeModel.setActive(true);
        dao.createNewOffice(officeModel, officeView.organizationId);
        return new SystemResponseView();
    }
}
