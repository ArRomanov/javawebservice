package my.homework.users;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class UsersDao {

    private final EntityManager em;

    @Autowired
    public UsersDao(EntityManager em) {
        this.em = em;
    }

    public void register(UsersModel user) {
        em.persist(user);
    }

    public UsersModel getUserByActivationCode(String activationCode) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<UsersModel> criteria = builder.createQuery(UsersModel.class);

        Root<UsersModel> rootCriteria = criteria.from(UsersModel.class);
        criteria.where(builder.equal(rootCriteria.get("activationCode"), activationCode));

        TypedQuery<UsersModel> query = em.createQuery(criteria);
        return query.getSingleResult();
    }

    public void updateUserStatus(long id, boolean status) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaUpdate<UsersModel> criteriaUpdate = builder.createCriteriaUpdate(UsersModel.class);

        Root<UsersModel> rootUser = criteriaUpdate.from(UsersModel.class);
        criteriaUpdate.set("isActive", status);
        criteriaUpdate.where(builder.equal(rootUser.get("id"), id));

        em.createQuery(criteriaUpdate).executeUpdate();
    }

    public UsersModel checkLoginAndPassw(String login, String password) {
        CriteriaBuilder builder = em.getCriteriaBuilder();
        CriteriaQuery<UsersModel> criteria = builder.createQuery(UsersModel.class);
        Root<UsersModel> rootCriteria = criteria.from(UsersModel.class);

        List<Predicate> predicates = new ArrayList<>();
        predicates.add(builder.equal(rootCriteria.get("login"), login));
        predicates.add(builder.equal(rootCriteria.get("hash"), password));
        criteria.select(rootCriteria).where(builder.and(predicates.toArray(new Predicate[]{})));
        return em.createQuery(criteria).getSingleResult();
    }
}
