package my.homework.users;

import javax.persistence.*;

/*
Пользователи сервиса
*/
@Entity
@Table(name = "users")
public class UsersModel {

    @Version
    private Integer version;

    /*
    Первичный ключ
    */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private long id;

    /*
    Имя
    */
    @Basic(optional = false)
    @Column(name = "name")
    private String name;

    /*
    Логин
    */
    @Basic(optional = false)
    @Column(name = "login")
    private String login;

    /*
    Хэш пароля
    */
    @Basic(optional = false)
    @Column(name = "hash")
    private String hash;

    /*
    Код для активации пользователя
    */
    @Basic(optional = false)
    @Column(name = "activation_code")
    private String activationCode;

    /*
    Статус активности
    */
    @Basic(optional = false)
    @Column(name = "is_active")
    private boolean isActive;

    public UsersModel() {

    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }

    public String getActivationCode() {
        return activationCode;
    }

    public void setActivationCode(String activationCode) {
        this.activationCode = activationCode;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }
}
