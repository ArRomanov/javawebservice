package my.homework.users;

import my.homework.SystemResponseView;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class UsersService {

    private final Logger log = LoggerFactory.getLogger(UsersService.class);

    private final UsersDao dao;

    @Autowired
    public UsersService(UsersDao dao) {
        this.dao = dao;
    }

    @Transactional
    public SystemResponseView register(UsersView view) {
        UsersModel user = new UsersModel();
        user.setLogin(view.login);
        user.setHash(view.password);
        user.setName(view.name);
        user.setActive(false);
        user.setActivationCode(generateActivationCode());
        try {
            dao.register(user);
            return new SystemResponseView();
        } catch (Exception ex) {
            return new SystemResponseView(ex.getMessage());
        }
    }

    private String generateActivationCode() {
        String activationCode = UUID.randomUUID().toString().substring(24);
        System.out.println(activationCode);
        return activationCode;
    }

    @Transactional
    public SystemResponseView activateUser(String activationCode) {
        try {
            UsersModel user = dao.getUserByActivationCode(activationCode);
            dao.updateUserStatus(user.getId(), true);
            return new SystemResponseView();
        } catch (EmptyResultDataAccessException ex) {
            return new SystemResponseView("Login or activation code is wrong");
        }
    }

    @Transactional
    public SystemResponseView logIn(UsersView view) {
        try {
            dao.checkLoginAndPassw(view.login, view.password);
            return new SystemResponseView();
        } catch (EmptyResultDataAccessException ex) {
            return new SystemResponseView("Login or password is wrong!");
        }
    }
}
