package my.homework.users;

import my.homework.SystemResponseView;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
@RequestMapping(value = "/api")
public class UserController {

    private UsersService usersService;

    @Autowired
    public UserController(UsersService usersService) {
        this.usersService = usersService;
    }

    @RequestMapping(value = "/register", method = {POST})
    public SystemResponseView registerNewUser(@RequestBody UsersView usersView) {
        return usersService.register(usersView);
    }

    @RequestMapping(value = "/activation", method = GET)
    public SystemResponseView activationUser(@RequestParam(value = "code") String code) {
        return usersService.activateUser(code);
    }

    @RequestMapping(value = "/login", method = {POST})
    public SystemResponseView logInByUser(@RequestBody UsersView usersView) {
        return usersService.logIn(usersView);
    }
}
