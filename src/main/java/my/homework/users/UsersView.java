package my.homework.users;

public class UsersView {

    public String login;

    public String name;

    public String password;

    public UsersView() {
    }

    public UsersView(String login, String password, String name) {
        this.login = login;
        this.password = password;
        this.name = name;
    }

    @Override
    public String toString() {
        return "{login:" + login + ";name:" + name + ";paswd" + password + "}";
    }
}
