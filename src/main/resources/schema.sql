CREATE TABLE IF NOT EXISTS organizations(
id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50) NOT NULL,
full_name VARCHAR(50),
inn INTEGER,
kpp INTEGER,
address VARCHAR (50),
phone INTEGER,
is_active BOOL,
version INTEGER
);

CREATE TABLE IF NOT EXISTS offices(
id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50),
phone INTEGER,
address VARCHAR(100),
organization_id INTEGER NOT NULL,
is_active BOOL,
version INTEGER
);

CREATE TABLE IF NOT EXISTS types_of_documents(
code INTEGER PRIMARY KEY NOT NULL,
name VARCHAR(50) NOT NULL,
);

CREATE TABLE IF NOT EXISTS countries_list(
code INTEGER PRIMARY KEY NOT NULL,
name VARCHAR(50) NOT NULL
);

CREATE TABLE IF NOT EXISTS employees(
id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
first_name VARCHAR(30) NOT NULL,
last_name VARCHAR(30),
middle_name VARCHAR(30),
phone INTEGER,
office_id INTEGER,
position VARCHAR(20),
doc_number INTEGER,
citizen_ship_code INTEGER,
is_identified BOOL,
version INTEGER
);

CREATE TABLE IF NOT EXISTS docs_of_employees(
id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
doc_code INTEGER,
doc_number INTEGER,
doc_date VARCHAR(20),
version INTEGER
);

CREATE TABLE IF NOT EXISTS users(
id INTEGER PRIMARY KEY AUTO_INCREMENT NOT NULL,
name VARCHAR(50),
login VARCHAR(50) NOT NULL,
hash VARCHAR(50) NOT NULL,
activation_code VARCHAR(50) NOT NULL,
is_active BOOL,
version INTEGER
);

ALTER TABLE offices ADD FOREIGN KEY (organization_id) REFERENCES organizations(id);
ALTER TABLE employees ADD FOREIGN KEY (office_id) REFERENCES offices(id);
ALTER TABLE employees ADD FOREIGN KEY (citizen_ship_code) REFERENCES countries_list(code);
ALTER TABLE employees ADD FOREIGN KEY (doc_number) REFERENCES docs_of_employees(id);
ALTER TABLE docs_of_employees ADD FOREIGN KEY (doc_code) REFERENCES types_of_documents(code);

CREATE INDEX IX_Organization_Id ON organizations (id);
CREATE INDEX IX_Office_Id ON offices (id);
CREATE INDEX IX_Docs_Code ON types_of_documents (code);
CREATE INDEX IX_Countries_code ON countries_list (code);
CREATE INDEX IX_Employee_Id ON employees (id);
CREATE INDEX IX_Employee_docs_Id ON docs_of_employees (id);
