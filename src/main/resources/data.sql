INSERT INTO organizations (name, full_name, inn, kpp, address, phone, is_active, version) VALUES ('First organization', 'OOO First', 1111, 1111, 'Penza 1', 0, true, 0);
INSERT INTO organizations (name, full_name, inn, kpp, address, phone, is_active, version) VALUES ('Second organization', 'OOO Second', 2222, 2222, 'Penza 2', 0512, true, 0);
INSERT INTO organizations (name, full_name, inn, kpp, address, phone, is_active, version) VALUES ('Third organization', 'OOO Third', 3333, 3333, 'Penza 3', 5456, true, 0);
INSERT INTO organizations (name, full_name, inn, kpp, address, phone, is_active, version) VALUES ('For deleting test', 'For deleting test', 9374, 4746, 'HZ', 1232, true, 0);

INSERT INTO offices (name, phone, organization_id, is_active, version) VALUES ('First office', 9546, 1, true, 0);
INSERT INTO offices (name, phone, organization_id, is_active, version) VALUES ('Second office', 0, 1, true, 0);
INSERT INTO offices (name, phone, organization_id, is_active, version) VALUES ('First office', 7654, 1, true, 0);
INSERT INTO offices (name, phone, organization_id, is_active, version) VALUES ('First office', 7621, 3, true, 0);
INSERT INTO offices (name, phone, organization_id, is_active, version) VALUES ('Second office', 0, 3, true, 0);

INSERT INTO types_of_documents (code, name) VALUES (1, 'Паспорт гражданина РФ');
INSERT INTO types_of_documents (code, name) VALUES (2, 'Водительское удостоверение');
INSERT INTO types_of_documents (code, name) VALUES (3, 'Военный билет');

INSERT INTO countries_list (code, name) VALUES (643, 'Россия');
INSERT INTO countries_list (code, name) VALUES (804, 'Украина');
INSERT INTO countries_list (code, name) VALUES (112, 'Беларусь');

INSERT INTO employees (first_name, last_name, middle_name, phone, office_id, position, citizen_ship_code, is_identified, version)
VALUES ('Василий', 'Иванов', null, 0, 1, 'Programmer', 643, true, 0);
INSERT INTO employees (first_name, last_name, middle_name, phone, office_id, position, citizen_ship_code, is_identified, version)
VALUES ('Петр', 'Ильин', 'Андреевич', 458759, 3, 'QA engineer', 643, true, 0);
INSERT INTO employees (first_name, last_name, middle_name, phone, office_id, position, citizen_ship_code, is_identified, version)
VALUES ('Николай', 'Володин', null, 0, 4, 'DBA', 804, true, 0);
INSERT INTO employees (first_name, last_name, middle_name, phone, office_id, position, citizen_ship_code, is_identified, version)
VALUES ('Антон', 'Корякин', null, 456789, 5, 'Manager', 112, true, 0);
INSERT INTO employees (first_name, last_name, middle_name, phone, position, citizen_ship_code, is_identified, version)
VALUES ('Test', 'Deleting', null, 0, 'lol', 112, true, 0);


INSERT INTO docs_of_employees (doc_code, doc_number, doc_date, version) VALUES (1,234567,'17-12-2015', 0);
INSERT INTO docs_of_employees (doc_code, doc_number, doc_date, version) VALUES (1,764556,'14-11-2010', 0);
INSERT INTO docs_of_employees (doc_code, doc_number, doc_date, version) VALUES (3,458754,'26-11-2017', 0);
INSERT INTO docs_of_employees (doc_code, doc_number, doc_date, version) VALUES (3,789521,'20-01-2011', 0);

update employees set doc_number=1 where id=1;
update employees set doc_number=2 where id=2;
update employees set doc_number=3 where id=3;
update employees set doc_number=4 where id=4;

insert into users (name, login, hash, activation_code, is_active, version) values ('name', 'test', 'test', '12345678', false, 0)
