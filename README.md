The project is for studying java spring framework and ORM (hibernate).

Running of unit and integration tests:
mvn clean test

Running of an application:
mvn spring-boot:run